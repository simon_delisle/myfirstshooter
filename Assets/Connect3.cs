﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connect3 : MonoBehaviour
{
    public float sensitivity = 100f ;

    //public Rigidbody RB;

    public Joystick RotateJoystick;

    public Joystick MoveJoystick;

    public Transform PlayerBody;

    float xRotation = 0f;

    // Update is called once per frame
    void Update()
    {
        float HorizontalX = RotateJoystick.Horizontal * sensitivity;

        

        xRotation -= HorizontalX;

        xRotation = Mathf.Clamp(xRotation,-90f,90f);

        transform.localRotation = Quaternion.Euler(xRotation,0f,0f);

        PlayerBody.Rotate(Vector3.up*HorizontalX);
        
        
    }
}
