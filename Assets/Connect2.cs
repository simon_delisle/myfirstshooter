﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connect2 : MonoBehaviour
{   
    public Transform RB;

    public Joystick moveJoystick;

    public float Speed=100f;

    

    void FixedUpdate()
    {   
        //We'll try Translate before rotate

        RB.Translate(moveJoystick.Horizontal * Time.deltaTime*Speed , 0 , 0 );

        RB.Translate( 0 , 0 ,moveJoystick.Vertical * Time.deltaTime * Speed );

        //RB.AddForce(moveJoystick.Vertical* Time.deltaTime*Speed,ForceMode.VelocityChange ) ;
        //RB.velocity = 0 ;
        //RB.angularVelocity = 0;
    }
    


}
